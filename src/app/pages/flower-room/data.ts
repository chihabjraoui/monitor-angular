import { ChartType } from './apex.model';

const strokedCircularGuage: ChartType = {
    chart: {
    	height: 300,
        type: 'radialBar',
		animations: {
			enabled: true,
			easing: 'easeinout',
			speed: 800,
			animateGradually: {
				enabled: true,
				delay: 150
			},
			dynamicAnimation: {
				enabled: true,
				speed: 350
			}
		}
    },
	// xaxis: {
	// 	type: 'string',
	// },
    plotOptions: {
        radialBar: {
            startAngle: -135,
            endAngle: 135,
            dataLabels: {
                name: {
                    fontSize: '16px',
                    color: '#fff',
                    offsetY: 80
                },
                value: {
                    offsetY: -10,
                    fontSize: '22px',
					color: '#fff',
                    formatter(val)
					{
                        return val + '%';
                    }
                }
            }
        }
    },
    fill: {
        gradient: {
            enabled: true,
            shade: 'dark',
            shadeIntensity: 0.15,
            inverseColors: false,
            opacityFrom: 1,
            opacityTo: 1,
            stops: [0, 50, 65, 91]
        },
    },
    // stroke: {
    //     dashArray: 4
    // },
    colors: ['#95759e'],
    series: [0],
    labels: [''],
    responsive: [{
        breakpoint: 380,
        options: {
            chart: {
                height: 280
            }
        }
    }],
	noData: {
		text: 'No Data Available',
		align: 'center',
		verticalAlign: 'middle',
		offsetX: 0,
		offsetY: 0,
		style: {
			color: undefined,
			fontSize: '14px',
			fontFamily: undefined
		}
	}
};
export {strokedCircularGuage};
