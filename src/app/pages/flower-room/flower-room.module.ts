import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FlowerRoomRoutingModule} from './flower-room-routing.module';
import {FlowerRoomComponent} from './flower-room.component';
import {UIModule} from '../../shared/ui/ui.module';
import {NgApexchartsModule} from 'ng-apexcharts';

@NgModule({
	declarations: [
		FlowerRoomComponent
	],
	imports: [
		CommonModule,
		FlowerRoomRoutingModule,

		UIModule,
		NgApexchartsModule,
	],
	schemas: [
		CUSTOM_ELEMENTS_SCHEMA
	]
})
export class FlowerRoomModule {
}
