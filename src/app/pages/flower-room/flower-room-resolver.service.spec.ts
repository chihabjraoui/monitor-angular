import { TestBed } from '@angular/core/testing';

import { FlowerRoomResolverService } from './flower-room-resolver.service';

describe('FlowerRoomResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FlowerRoomResolverService = TestBed.get(FlowerRoomResolverService);
    expect(service).toBeTruthy();
  });
});
