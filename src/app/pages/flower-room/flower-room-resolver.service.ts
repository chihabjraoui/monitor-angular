import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {FlowerRoomService} from './flower-room.service';

@Injectable({
	providedIn: 'root'
})
export class FlowerRoomResolverService implements Resolve<any>
{

	constructor(private service: FlowerRoomService)
	{
	}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
	{
		return null;
	}
}
