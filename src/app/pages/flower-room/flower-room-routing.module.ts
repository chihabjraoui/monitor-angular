import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FlowerRoomComponent} from './flower-room.component';

const routes: Routes = [
	{
		path: '',
		component: FlowerRoomComponent
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class FlowerRoomRoutingModule {
}
