import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowerRoomComponent } from './flower-room.component';

describe('FlowerRoomComponent', () => {
  let component: FlowerRoomComponent;
  let fixture: ComponentFixture<FlowerRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlowerRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlowerRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
