import {Component, OnDestroy, OnInit} from '@angular/core';
import {ChartType} from '../chart/apex/apex.model';
import {strokedCircularGuage} from './data';
import {FlowerRoomService} from './flower-room.service';
import {concat, interval, Observable, of, range, Subscription, timer} from 'rxjs';
import {concatMap, map, startWith, switchMap, take, tap} from 'rxjs/internal/operators';
import {ITopic} from '../../core/models/topic.model';

@Component({
	selector: 'app-flower-room',
	templateUrl: './flower-room.component.html',
	styleUrls: ['./flower-room.component.scss']
})
export class FlowerRoomComponent implements OnInit, OnDestroy
{
	gaugeChart: ChartType;

	flowerRoomTopics;

	values = [];

	subscriptions: Subscription[] = [];

	interval;

	constructor(private service: FlowerRoomService)
	{
		this.gaugeChart = strokedCircularGuage;

		let topicsIds = [];

		service.getFlowerRoomTopics().subscribe((topics) =>
		{
			topics.forEach(topic =>
			{
				this.values[topic.id] = [0];
				topicsIds.push(topic.id);
			});

			this.flowerRoomTopics = topics;

			interval(10000).pipe(
				startWith(0),
				switchMap(() => service.getTopicsValues(topicsIds))
			).subscribe((values: any[]) =>
			{
				values.forEach((value: any) =>
				{
					this.values[value.topic_id] = service.convertValue(value);
				})
			});

			// topics.forEach((topic) =>
			// {
			// 	this.subscriptions.push(service.getTopicValue(topic.id).subscribe((value: any) =>
			// 	{
			// 		console.log(value);
			// 	}));
			// });
		});
	}

	ngOnInit()
	{
	}

	ngOnDestroy(): void
	{
		clearInterval(this.interval);
	}
}
