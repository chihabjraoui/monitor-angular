import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UrlProviderService} from '../../core/url-provider/url-provider.service';
import {ITopic} from '../../core/models/topic.model';

@Injectable({
	providedIn: 'root'
})
export class FlowerRoomService
{

	constructor(private http: HttpClient,
				private urlProvider: UrlProviderService)
	{
	}

	getFlowerRoomTopics()
	{
		const url = this.urlProvider.getApiEndpoint('topics', 'getTopicsByRoomName', 'flower');
		return this.http.get<ITopic[]>(url);
	}

	getTopicsValues(topicsIds)
	{
		const url = this.urlProvider.getApiEndpoint('values', 'topicsLastValue');
		return this.http.post(url, {topicsIds: topicsIds});
	}

	convertValue(lastValue): any
	{
		if(lastValue !== null && lastValue !== undefined)
		{
			let result: any;

			switch (lastValue.type)
			{
				case 'number':
					result = parseFloat(lastValue.value);
					break;

				case 'boolean':
					result = !!lastValue.value;
					break;

				default: result = lastValue.value;
			}

			return [result]
		}

		return [0];
	}
}
