import { TestBed } from '@angular/core/testing';

import { FlowerRoomService } from './flower-room.service';

describe('FlowerRoomService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FlowerRoomService = TestBed.get(FlowerRoomService);
    expect(service).toBeTruthy();
  });
});
