export interface ITopic
{
	id?: number;
	key?: string;
	topic?: string;
	values?: any[];
}
